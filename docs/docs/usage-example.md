```js
enableEvent_Render(true);

addEventHandler("onInit", function() {
    mumble.openConnection("yourip", 64738, "password");
);

addEventHandler("mumble:onPlayerTalking", function(playerId) 
{
	print("Player with ID - " + playerId + " is talking!");
});

addEventHandler("onPlayerCreate", function(playerId) 
{
    if (playerId == heroId) 
    {
        setTimer(function() 
        {
            mumble.toggleOutput(true); // Enable audio output

            mumble.toggle3dVoice(true); // Enable 3d voice
	        mumble.setVoiceRange(2000); // Set 3d voice range

            mumble.toggleTargetList(true); // Enable targetList
        }
        , 500, 1);
    }
});

local voice = false, tick = getTickCount(), maxSlots = getMaxSlots();

addEventHandler("onRender", function()
{
    local heroPos = getPlayerPosition(heroId), now = getTickCount();
    
    if(now - tick >= 50)
    {
        mumble.setVoicePosition(heroPos.x, heroPos.y, heroPos.z);

        for(local i = 0; i < maxSlots; i++)
        {
            if(isPlayerCreated(i))
            {
                local pos = getPlayerPosition(i);
                
                if(getDistance3d(heroPos.x, heroPos.y, heroPos.z, pos.x, pos.y,pos.z) <= 2000)
                {
                    mumble.addVoiceTarget(i);
                }    
                else
                {
                    mumble.removeVoiceTarget(i);
                }
            }
        }
        
        tick = now;
    }

    if(isKeyPressed(KEY_V))
    {
        if(voice == false)
        {
            mumble.toggleInput(true);
            voice = true;
        }
    } 
    else 
    {
        if(voice) 
        {
            mumble.toggleInput(false);
            voice = false;
        }
    }
});
```
