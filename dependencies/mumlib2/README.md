## Build

* Dependencies
  * asio
  * Protobuf
  * OpenSSL
  * Opus
  * portaudio
  
## Original authors

* Michał Słomkowski (original author)
* Mikhail Paulyshka
* Auzan Muhammad
* Benedikt Wildenhain
* HeroCC
* Hiroshi Takey F
* Hunter N. Morgan
* Matthias Larisch
* Patrik Dahlström
* Scott Hardin
* Zura/KimiroV

The library contains code from following 3rd party projects:

* mumble: https://github.com/mumble-voip/mumble
* libmumble: https://github.com/cornejo/libmumble
