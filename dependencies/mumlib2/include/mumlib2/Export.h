
#ifndef MUMLIB2_EXPORT_H
#define MUMLIB2_EXPORT_H

#ifdef MUMLIB2_STATIC_DEFINE
#  define MUMLIB2_EXPORT
#  define MUMLIB2_NO_EXPORT
#else
#  ifndef MUMLIB2_EXPORT
#    ifdef mumlib2_EXPORTS
        /* We are building this library */
#      define MUMLIB2_EXPORT 
#    else
        /* We are using this library */
#      define MUMLIB2_EXPORT 
#    endif
#  endif

#  ifndef MUMLIB2_NO_EXPORT
#    define MUMLIB2_NO_EXPORT 
#  endif
#endif

#ifndef MUMLIB2_DEPRECATED
#  define MUMLIB2_DEPRECATED __declspec(deprecated)
#endif

#ifndef MUMLIB2_DEPRECATED_EXPORT
#  define MUMLIB2_DEPRECATED_EXPORT MUMLIB2_EXPORT MUMLIB2_DEPRECATED
#endif

#ifndef MUMLIB2_DEPRECATED_NO_EXPORT
#  define MUMLIB2_DEPRECATED_NO_EXPORT MUMLIB2_NO_EXPORT MUMLIB2_DEPRECATED
#endif

#if 0 /* DEFINE_NO_DEPRECATED */
#  ifndef MUMLIB2_NO_DEPRECATED
#    define MUMLIB2_NO_DEPRECATED
#  endif
#endif

#endif /* MUMLIB2_EXPORT_H */
