# MUMBLE

This library introduces voice chat solution for Gothic Online, from now using mumble you can integrate communication for your team or role play sesssion.
Currently library is in development state, there might be some bugs, but I am working on it. If you find anything uncertain please inform me via issue or contact me on discord.

## INSTALL

**_NOTE:_** Client modules aren't downloaded by default by g2o server.  
You have to put them manually into: `Game/Multiplayer/Modules` directory.

In order to install the module you can either [download the prebuilt binary](../../releases) from releases, or [build the module yourself](#build-instructions).  

## LOAD

To load the module, you have to put `<module>` tag into .xml server configuration.  
Below you can find more info about this tag **attributes**.

```xml
<!--The path to the module relative to .xml file-->
<!--Client-side module must be placed in the exact game path from which it is loaded on the server, e.g: -->
<!--Loading module on s-side: server/MyServerName/sqmodule.dll-->
<!--Will cause the module to be searched in: game/Multiplayer/Modules/MyServerName/sqmodule.dll-->
<!--[required]--> src="path/to/the/module"

<!--[required]--> type="client"|"server"

<!--By default module will be loaded without checksum validation-->
<!--useful when you want to load only specific version of the module-->
<!--[optional]--> md5="1a79a4d60de6718e8e5b326e338ae533"

<!--By default every module is required, you can override this by setting required to false-->
<!--Useful for creating optional modules-->
<!--[optional]--> required=true|false
```

## DOCS

The detailed online documentation of the module can be found here:  
https://tomasz-k.gitlab.io/mumble/

Offline documentation version can be downloaded from [releases](../../releases).

## TODO

* improved positional audio
* thread bug fixes
* docs 

## AUTHOR

* Zura/KimiorV

The library contains code from following 3rd party projects:

* mumble: https://github.com/mumble-voip/mumble
* libmumble: https://github.com/cornejo/libmumble

## CONTACT 

If you have questions you can add me to the frinds on discord, my id - kimiorv