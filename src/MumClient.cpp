#include "MumClient.h"

#include <Windows.h>

MumClient* MumClient::_obj = nullptr;

MumClient::MumClient() : _voiceBuffer { 100 }
{
    _audioStream.init();
}

MumClient::~MumClient()
{
    if (_clientThread && _clientThread->joinable())
    {
        _killThread = true;

        _mumlib->disconnect();
        _clientThread->join();
    }

    std::cout << "MumClient:: Destroy client..." << std::endl;
}

void MumClient::close()
{
    delete _obj;
}

void MumClient::setMumLib(mumlib2::Mumlib2* mum)
{
    _mumlib = mum;
}

mumlib2::Mumlib2* MumClient::getMumlib()
{
    return _mumlib;
}

void MumClient::setPlayerId(int playerId)
{
    _playerId = playerId;
}

int MumClient::getPlayerId()
{
    if (_obj)
    {
        return _playerId;
    }

    return -1;
}

void MumClient::exit()
{
    _audioStream.exit();
}

bool MumClient::openConnection(std::string serverIp, uint16_t port, std::string password)
{
    if (_clientThread)
        return false;

    _serverIp = serverIp;
    _serverPort = port;
    _serverPassword = password;

    _killThread = false;
    _clientThread = std::make_unique<std::thread>(&MumClient::worker, this);

    return true;
}

bool MumClient::closeConnection()
{
    if (_clientThread && _clientThread->joinable())
    {
        _killThread = true;
     
        _mumlib->disconnect();
        _clientThread->join();

        return true;
    }

    return false;
}

void MumClient::toggleInput(bool value)
{
    _audioStream.toggleInput(value);
}

void MumClient::toggleOutput(bool value)
{
    _audioStream.toggleOutput(value);
}

void MumClient::pushToOutputQueue(const int16_t* outputBuffer, size_t sampleCount, const std::array<float, 3>& position)
{
    _audioStream.pushToOutputQueue(outputBuffer, sampleCount, position);
}

void MumClient::setOutputVolume(double volume)
{
    _audioStream.setOutputVolume(volume);
}

double MumClient::getOutputVolume()
{
    return _audioStream.getOutputVolume();
}

double MumClient::getInputVolume()
{
    return _audioStream.getInputVolume();
}

void MumClient::playerUpdate(int32_t sessionId, int32_t playerId)
{
    _targetList.playerUpdate(sessionId, playerId);
}

void MumClient::playerDisconnect(int32_t sessionId)
{
    _targetList.playerDisconnect(sessionId);
}

void MumClient::playerTalking(int32_t playerId, bool is_last)
{
    _voiceBuffer.enqueue(std::make_pair(playerId, is_last));
}

void MumClient::executeAsync()
{
    this->_asyncPost.worker();

    if (_voiceBuffer.size_approx() == 0)
        return;

    std::pair<int, bool> item;

    while (_voiceBuffer.try_dequeue(item))
    {
        Sqrat::RootTable().GetFunction("callEvent").Execute("mumble:onPlayerTalking", (int32_t)item.first, (bool)item.second);
    }
}

void MumClient::addVoiceTarget(int32_t playerId)
{
    _targetList.addVoiceTarget(playerId);
}

void MumClient::removeVoiceTarget(int32_t playerId)
{
    _targetList.removeVoiceTarget(playerId);
}

std::vector<int> MumClient::getVoiceTargets()
{
    return _targetList.getTargetList();
}

void MumClient::toggleTargetList(bool value)
{
    _targetList.toggle(value);
}

bool MumClient::isTargetListToggled()
{
    return _targetList.isToggled();
}

AudioTargetList& MumClient::getAudioTargetList()
{
    return _targetList;
}

void MumClient::mutePlayer(int playerId, bool mute)
{
    _targetList.mutePlayerVoice(playerId, mute);
}

bool MumClient::isPlayerMuted(int playerId)
{
    return _targetList.isPlayerMuted(playerId);
}

void MumClient::setVoicePosition(float x, float y, float z)
{
    _audioPosition.setVoicePosition(x, y, z);
}

void MumClient::setVoiceRange(int range)
{
    _audioPosition.setVoiceRange(range);
}

int MumClient::getVoiceRange()
{
    return _audioPosition.getVoiceRange();
}

AudioPosition::Position MumClient::getVoicePosition()
{
    return _audioPosition.getVoicePosition();
}

void MumClient::toggle3dVoice(bool value)
{
    _audioPosition.toggle3dVoice(value);
}

bool MumClient::is3dVoiceEnabled()
{
    return _audioPosition.isEnabled();
}

AudioPosition& MumClient::getAudioPosition()
{
    return _audioPosition;
}

void MumClient::enqueuePost(std::function<void()> function)
{
    _asyncPost.enqueuePost(function);
}

void MumClient::enqueueInputStream(std::function<void()> function)
{
    _asyncPost.enqueueInputStream(function);
}

void MumClient::joinChannel(std::string name)
{
    _asyncPost.joinChannel(name);
}

MumAsyncPost& MumClient::getAsyncPost()
{
    return _asyncPost;
}

void MumClient::worker()
{
    MumCallback callback;
    mumlib2::Mumlib2 mumlib(callback);

    this->setMumLib(&mumlib);

    while (true)
    {
        try
        {            
            if(mumlib.connect(_serverIp, _serverPort, std::to_string(this->getPlayerId()), _serverPassword))
            {
                mumlib.run();
            }
            else
            {
                std::cout << "Cannot reconnect to the mumble server! Next attempt in 5 seconds..." << std::endl;
            }
        }
        catch (mumlib2::TransportException& exp)
        {
            std::cout << exp.what() << std::endl;
        }

        if (this->_killThread)
            break;

        std::this_thread::sleep_for(std::chrono::seconds(5));
    }
}

MumClient* MumClient::getInstance()
{
    if (_obj == nullptr)
    {
        _obj = new MumClient();
    }

    return _obj;
}