#pragma once

#include <iostream>
#include <thread>
#include <readerwriterqueue.h>
#include <mumlib2.h>

class MumAsyncPost
{
private:
    moodycamel::ReaderWriterQueue<std::function<void()>> _postBuffer;

public:
	MumAsyncPost();
    
    void enqueueInputStream(std::function<void()> function);
    void enqueuePost(std::function<void()> function);
    void joinChannel(std::string name);

    void worker();
};

