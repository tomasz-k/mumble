#define WIN32_LEAN_AND_MEAN

#include <iostream>
#include <sqrat.h>

#include "MumClient.h"
#include "MumBind.h"

#include "sq_callback.h"

void addEventHandler(const char* eventName, SQFUNCTION closure, unsigned int priority = 9999)
{
    using namespace SqModule;

    Sqrat::Function sq_addEventHandler = Sqrat::RootTable().GetFunction("addEventHandler");

    if (sq_addEventHandler.IsNull())
        return;

    HSQOBJECT closureHandle;

    sq_newclosure(vm, closure, 0);
    sq_getstackobj(vm, -1, &closureHandle);

    Sqrat::Function func(vm, Sqrat::RootTable().GetObject(), closureHandle);
    sq_addEventHandler(eventName, func, priority);

    sq_pop(vm, 1);
}

extern "C" SQRESULT SQRAT_API sqmodule_load(HSQUIRRELVM vm, HSQAPI api)
{
    SqModule::Initialize(vm, api);
    Sqrat::DefaultVM::Set(vm);
    
    MumClient* client = MumClient::getInstance();

    if (client)
    {
        int playerId = Sqrat::ConstTable(SqModule::vm).GetSlot("heroId").Cast<int>();

        std::cout << "MumClient:: User joined with ID: " << playerId << std::endl;

        if (playerId >= 0 && playerId < 512)
        {
            client->setPlayerId(playerId);
            MumBind::bind();

            Sqrat::RootTable(SqModule::vm).GetFunction("enableEvent_Render").Execute<bool>(true);

            addEventHandler("onRender", sq_onRender, 0xFFFFFFFF);
            addEventHandler("onExit", sq_onExit, 0xFFFFFFFF);
            
            std::atexit(MumClient::close);
        }

        return SQ_OK;
    }
    
    return SQ_ERROR;
}
