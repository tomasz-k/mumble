#include "MumCallback.h"
#include "MumClient.h"

void MumCallback::audio(int target, int sessionId, 
    int sequenceNumber, 
    bool is_last,
    const std::array<float, 3>& position,
    const int16_t* pcm_data,
    size_t samples_count) 
{
    MumClient* client = MumClient::getInstance();

    if (client)
    {
        int32_t internalId = stoi(client->getMumlib()->UserGet(sessionId)->name);

        if (client->isPlayerMuted(internalId))
            return;

        client->pushToOutputQueue(pcm_data, samples_count, position);
    
        try
        {
            // Regulus
            client->playerTalking(internalId, is_last);
        }
        catch (std::exception err)
        {
            std::cout << err.what() << std::endl;
        }
    }
}

void MumCallback::userState(
    int32_t session,
    int32_t actor,
    std::string name,
    int32_t user_id,
    int32_t channel_id,
    int32_t mute,
    int32_t deaf,
    int32_t suppress,
    int32_t self_mute,
    int32_t self_deaf,
    std::string comment,
    int32_t priority_speaker,
    int32_t recording)
{
    try
    {
        MumClient::getInstance()->playerUpdate(session, stoi(name));
    }
    catch (std::exception err)
    {
        std::cout << err.what() << std::endl;
    }
}

void MumCallback::userRemove(uint32_t session,
    int32_t actor,
    std::string reason,
    bool ban)
{
    try
    {
        MumClient::getInstance()->playerDisconnect(session);
    }
    catch (std::exception err)
    {
        std::cout << err.what() << std::endl;
    }
}