#pragma once

#include <iostream>
#include <string>

#include <mumlib2.h>

class MumCallback : public mumlib2::Callback {
public:
    virtual void audio(int target, int sessionId, int sequenceNumber, bool is_last, const std::array<float, 3U>& position, const int16_t* pcm_data, size_t samples_count) override;

    virtual void userState(
        int32_t session,
        int32_t actor,
        std::string name,
        int32_t user_id,
        int32_t channel_id,
        int32_t mute,
        int32_t deaf,
        int32_t suppress,
        int32_t self_mute,
        int32_t self_deaf,
        std::string comment,
        int32_t priority_speaker,
        int32_t recording) override;

    virtual void userRemove(uint32_t session,
        int32_t actor,
        std::string reason,
        bool ban) override;
};