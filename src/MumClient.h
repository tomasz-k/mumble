#ifndef _MUMCLIENT_H
#define _MUMCLIENT_H

#include <iostream>
#include <thread>
#include <mutex>

#include <mumlib2.h>
#include <sqrat.h>
#include <readerwriterqueue.h>

#include "MumAsyncPost.h"
#include "MumCallback.h"

#include "audio/AudioPosition.h"
#include "audio/AudioStream.h"
#include "audio/AudioTargetList.h"

class MumClient
{
protected:
    static MumClient* _obj;

private:
    std::unique_ptr<std::thread> _clientThread;
    bool _killThread = false;

    AudioPosition _audioPosition;
    AudioTargetList _targetList;

    AudioStream _audioStream;
    MumAsyncPost _asyncPost;

    std::string _serverIp = "127.0.0.1";
    uint16_t _serverPort = 64738;
    std::string _serverPassword = "password";

    mumlib2::Mumlib2* _mumlib;
    std::atomic<int> _playerId;

    moodycamel::ReaderWriterQueue<std::pair<int, bool>> _voiceBuffer;
public:
    MumClient();
    ~MumClient();

    static void close();

    void setMumLib(mumlib2::Mumlib2* mum);
    mumlib2::Mumlib2* getMumlib();

    void setPlayerId(int playerId);
    int getPlayerId();

    void exit();

    bool openConnection(std::string serverIp, uint16_t port, std::string password = "");
    bool closeConnection();

    void toggleInput(bool value);
    void toggleOutput(bool value);
    void pushToOutputQueue(const int16_t* outputBuffer, size_t sampleCount, const std::array<float, 3>& position);
    void setOutputVolume(double volume);
    double getOutputVolume();
    double getInputVolume();

    void playerUpdate(int32_t sessionId, int32_t playerId);
    void playerDisconnect(int32_t sessionId);
    void playerTalking(int32_t playerId, bool is_last);

    void executeAsync();

    void addVoiceTarget(int32_t playerId);
    void removeVoiceTarget(int32_t playerId);
    std::vector<int> getVoiceTargets();
    void toggleTargetList(bool toggle);
    bool isTargetListToggled();
    AudioTargetList& getAudioTargetList();

    void mutePlayer(int playerId, bool mute);
    bool isPlayerMuted(int playerId);

    void setVoicePosition(float x, float y, float z);
    AudioPosition::Position getVoicePosition();
    void setVoiceRange(int range);
    int getVoiceRange();
    void toggle3dVoice(bool value);
    bool is3dVoiceEnabled();
    AudioPosition& getAudioPosition();

    void enqueuePost(std::function<void()> function);
    void enqueueInputStream(std::function<void()> function);
    void joinChannel(std::string name);
    MumAsyncPost& getAsyncPost();

    void worker();

    static MumClient* getInstance();
};

#endif