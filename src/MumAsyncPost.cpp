#include "MumAsyncPost.h"
#include "MumClient.h"

MumAsyncPost::MumAsyncPost() : _postBuffer { 200 }
{
}

void MumAsyncPost::enqueueInputStream(std::function<void()> function)
{
    _postBuffer.enqueue(function);
}

void MumAsyncPost::enqueuePost(std::function<void()> function)
{
    mumlib2::Mumlib2* mumlib = MumClient::getInstance()->getMumlib();
    if (!mumlib) return;

    mumlib->sendFunction(function);
}

void MumAsyncPost::joinChannel(std::string name)
{
    mumlib2::Mumlib2* mumlib = MumClient::getInstance()->getMumlib();
    if (!mumlib) return;
 
    mumlib->sendFunction([mumlib, name]()
        {
            mumlib->ChannelJoin(name);
        }
    );
}

void MumAsyncPost::worker()
{
    mumlib2::Mumlib2* mumlib = MumClient::getInstance()->getMumlib();
    if (!mumlib) return;

    std::function<void()> function;

    if (_postBuffer.try_dequeue(function))
    {
        mumlib->sendFunction(function);
    }
}