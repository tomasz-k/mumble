#pragma once

#include <sqrat.h>

namespace MumBind 
{
    void addEvent(const char* eventName);
    void bind();

    // MumClient
    SQInteger openConnection(HSQUIRRELVM vm);
    SQInteger closeConnection(HSQUIRRELVM vm);
    // MumAsyncPost
    SQInteger joinChannel(HSQUIRRELVM vm);
    // AudioStream
    SQInteger toggleInput(HSQUIRRELVM vm);
    SQInteger toggleOutput(HSQUIRRELVM vm);
    SQInteger setOutputVolume(HSQUIRRELVM vm);
    SQInteger getOutputVolume(HSQUIRRELVM vm);
    SQInteger getInputVolume(HSQUIRRELVM vm);
    // AudioTargetList
    SQInteger addVoiceTarget(HSQUIRRELVM vm);
    SQInteger removeVoiceTarget(HSQUIRRELVM vm);
    SQInteger getVoiceTargets(HSQUIRRELVM vm);
    SQInteger toggleTargetList(HSQUIRRELVM vm);
    SQInteger isTargetListToggled(HSQUIRRELVM vm);
    // AudioPosition
    SQInteger setVoicePosition(HSQUIRRELVM vm);
    SQInteger setVoiceRange(HSQUIRRELVM vm);
    SQInteger toggle3dVoice(HSQUIRRELVM vm);
    SQInteger is3dVoiceEnabled(HSQUIRRELVM vm);
    // Mute
    SQInteger mutePlayer(HSQUIRRELVM vm);
};