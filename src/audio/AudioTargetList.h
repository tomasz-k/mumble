#pragma once

#include <iostream>
#include <vector>
#include <mutex>
#include <atomic>

#define TARGET_ID 3

class AudioTargetList
{
public:
	struct Player
	{
		int _playerId = 0;
		int _sessionId = 0;

		bool _isTarget = false;
		bool _isMuted = false;
	};

private:
	std::vector<AudioTargetList::Player> _playerList;
	std::atomic<bool> _isToggled = false;

	mutable std::mutex _mutex;

public:
	AudioTargetList();
	~AudioTargetList();

	void playerUpdate(int sessionId, int playerId);
	void playerDisconnect(int sessionId);

	bool addVoiceTarget(int playerId);
	bool removeVoiceTarget(int playerId);

	void mutePlayerVoice(int playerId, bool muted);
	bool isPlayerMuted(int playerId);

	std::vector<int> getTargetList() const;

	void toggle(bool value);
	bool isToggled();
};