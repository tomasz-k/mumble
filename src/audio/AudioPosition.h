#pragma once

#include <atomic>
#include <iostream>

class AudioPosition
{
public:
    struct Position
    {
        float x = 0.0;
        float y = 0.0;
        float z = 0.0;
    };

private:
    std::atomic<AudioPosition::Position> _voicePosition;
    std::atomic<int> _voiceRange;

    std::atomic<bool> _toggle3dVoice;

public:
    AudioPosition();

    void setVoicePosition(float x, float y, float z);
    AudioPosition::Position getVoicePosition();

    void setVoiceRange(int range);
    int getVoiceRange();

    void toggle3dVoice(bool toggle);
    bool isEnabled();

    static double getDistance(int x0, int y0, int z0, int x1, int y1, int z1);
};