#include "AudioStream.h"
#include "MumClient.h"

AudioStream::AudioStream() : _outputBuffer { 200 }, _manualExit(false), _toggleInput(false), _toggleOutput(false), _outputVolume(1.0), _inputVolume(0.0)
{
}

AudioStream::~AudioStream()
{ 
    if (_manualExit == false)
        return;
 
    PaError err = Pa_Terminate();

    if (err != paNoError)
    {
        std::cerr << "Error terminating PortAudio: " << Pa_GetErrorText(err) << std::endl;
    }
}

void AudioStream::exit()
{
    if (stream)
    {
        PaError err = Pa_StopStream(stream);

        if (err != paNoError)
        {
            std::cerr << "Error stopping PortAudio stream: " << Pa_GetErrorText(err) << std::endl;
        }

        err = Pa_CloseStream(stream);

        if (err != paNoError)
        {
            std::cerr << "Error closing PortAudio stream: " << Pa_GetErrorText(err) << std::endl;
        }

        _manualExit = true;
    }
}

void AudioStream::toggleInput(bool toggle)
{
    _toggleInput = toggle;
}

void AudioStream::toggleOutput(bool toggle)
{
    _toggleOutput = toggle;
}

void AudioStream::setOutputVolume(double volume)
{
    _outputVolume = std::max(0.0, std::min(volume, 1.0));
}

double AudioStream::getOutputVolume()
{
    return _outputVolume;
}

void AudioStream::setInputVolume(double volume)
{
    _inputVolume = volume;
}

double AudioStream::getInputVolume()
{
    return _inputVolume;
}

int AudioStream::callback(const void* inputBuffer, void* outputBuffer,
    unsigned long framesPerBuffer,
    const PaStreamCallbackTimeInfo* timeInfo,
    PaStreamCallbackFlags statusFlags,
    void* data)
{
    if (framesPerBuffer <= 0)
        return paContinue;

    AudioStream* ptr = static_cast<AudioStream*>(data);

    if (inputBuffer)
    {
        const int16_t* buffer = static_cast<const int16_t*>(inputBuffer);
        double bufferRMS = 0.0;

        for (unsigned long i = 0; i < framesPerBuffer; ++i)
        {
            bufferRMS += static_cast<double>(buffer[i]) * static_cast<double>(buffer[i]);
        }

        ptr->setInputVolume(std::sqrt(bufferRMS / static_cast<double>(framesPerBuffer)));

        if (ptr->_toggleInput)
        {
            ptr->inputStreamCallback(buffer, framesPerBuffer);
        }
    }

    if (ptr->_toggleOutput && outputBuffer)
    {
        ptr->outputStreamCallback(outputBuffer, framesPerBuffer);
    }

    return paContinue;
}

void AudioStream::inputStreamCallback(const int16_t* inputBuffer, size_t framesPerBuffer)
{
    AudioStream::Packet packet;

    packet._buffer = new int16_t[framesPerBuffer];
    packet._bufferSize = framesPerBuffer;

    memset(packet._buffer, 0, framesPerBuffer * sizeof(int16_t));
    memcpy(packet._buffer, inputBuffer, framesPerBuffer * sizeof(int16_t));

    MumClient::getInstance()->enqueueInputStream([packet]()
        {
            MumClient* client = MumClient::getInstance();

            try
            {
                mumlib2::Mumlib2* mumlib = client->getMumlib();
                  
                if (client->isTargetListToggled())
                {
                    if (client->is3dVoiceEnabled())
                    {
                        AudioPosition::Position position = MumClient::getInstance()->getVoicePosition();
                        mumlib->sendAudioDataTarget(TARGET_ID, packet._buffer, packet._bufferSize, true, position.x, position.y, position.z);
                    }   
                    else
                    {                
                        mumlib->sendAudioDataTarget(TARGET_ID, packet._buffer, packet._bufferSize);
                    }
                }
                else
                {
                    if (client->is3dVoiceEnabled())
                    {
                        AudioPosition::Position position = MumClient::getInstance()->getVoicePosition();
                        mumlib->sendAudioDataPosition(position.x, position.y, position.z, packet._buffer, packet._bufferSize);
                    }
                    else
                    {
                        mumlib->sendAudioData(packet._buffer, packet._bufferSize);
                    }
                }
            }
            catch (const std::exception& ex)
            {
                std::cout << ex.what() << std::endl;
            }

            delete[] packet._buffer;
        }
    );
}

void AudioStream::outputStreamCallback(void* outputBuffer, size_t framesPerBuffer)
{
    if (_outputBuffer.size_approx() == 0)
    {
        memset(outputBuffer, 0, sizeof(int16_t) * framesPerBuffer * 2);
        return;
    }

    AudioStream::Packet buffer;

    if (_outputBuffer.try_dequeue(buffer))
    {
        MumClient* client = MumClient::getInstance();
        int16_t* stereoBuffer = static_cast<int16_t*>(outputBuffer);
        
        if (client->is3dVoiceEnabled())
        {
            AudioPosition::Position tPosition = client->getVoicePosition();

            double volumeScale = 1.0 - (AudioPosition::getDistance(tPosition.x, tPosition.y, tPosition.z, buffer._position[0], buffer._position[1], buffer._position[2]) / (double)client->getVoiceRange());
            volumeScale = _outputVolume * std::max(0.0, volumeScale);

            for (size_t i = 0; i < buffer._bufferSize; ++i)
            {
                stereoBuffer[i * 2] = static_cast<int16_t>(buffer._buffer[i] * volumeScale);
                stereoBuffer[i * 2 + 1] = static_cast<int16_t>(buffer._buffer[i] * volumeScale);
            }
        }
        else
        {
            for (size_t i = 0; i < buffer._bufferSize; ++i)
            {
                stereoBuffer[i * 2] = buffer._buffer[i];
                stereoBuffer[i * 2 + 1] = buffer._buffer[i];
            }
        }
    }
}

void AudioStream::pushToOutputQueue(const int16_t* outputBuffer, size_t sampleCount, const std::array<float, 3>& position)
{
    AudioStream::Packet packet;

    packet._buffer = new int16_t[sampleCount];
    packet._bufferSize = sampleCount;
    packet._position = position;

    memset(packet._buffer, 0, sampleCount * sizeof(int16_t));
    memcpy(packet._buffer, outputBuffer, sampleCount * sizeof(int16_t));

    _outputBuffer.emplace(packet);
}

void AudioStream::init()
{
    PaError err = Pa_Initialize();

    if (err != paNoError)
    {
        std::cout << "PortAudio initialization error: " << Pa_GetErrorText(err) << std::endl;
    }

    PaDeviceIndex inputDevice = Pa_GetDefaultInputDevice();
    PaStreamParameters inputParameters;

    if(inputDevice != paNoDevice)
    {
        inputParameters.device = inputDevice;
        inputParameters.channelCount = 1;
        inputParameters.sampleFormat = paInt16;
        inputParameters.suggestedLatency = Pa_GetDeviceInfo(inputParameters.device)->defaultLowInputLatency;
        inputParameters.hostApiSpecificStreamInfo = nullptr;
    }

    PaDeviceIndex outputDevice = Pa_GetDefaultOutputDevice();
    PaStreamParameters outputParameters;

    if (outputDevice != paNoDevice)
    {
        outputParameters.device = outputDevice;
        outputParameters.channelCount = 2;
        outputParameters.sampleFormat = paInt16;
        outputParameters.suggestedLatency = Pa_GetDeviceInfo(outputParameters.device)->defaultLowOutputLatency;
        outputParameters.hostApiSpecificStreamInfo = nullptr;
    }

    err = Pa_OpenStream(&stream, inputDevice != paNoDevice ? &inputParameters : nullptr, outputDevice != paNoDevice ? &outputParameters : nullptr, SAMPLE_RATE, FRAMES_PER_BUFFER, paNoFlag, AudioStream::callback, this);

    if (err != paNoError)
    {
        std::cout << "PortAudio stream error: " << Pa_GetErrorText(err) << std::endl;
    }

    err = Pa_StartStream(stream);

    if (err != paNoError)
    {
        std::cout << "PortAudio start stream error: " << Pa_GetErrorText(err) << std::endl;
    }
}