#include "AudioPosition.h"

AudioPosition::AudioPosition()
{
    _voicePosition = AudioPosition::Position(0.0, 0.0, 0.0);
    _voiceRange = 0;

    _toggle3dVoice = false;
}

void AudioPosition::setVoicePosition(float x, float y, float z)
{
    _voicePosition = AudioPosition::Position(x, y, z);
}

AudioPosition::Position AudioPosition::getVoicePosition()
{
    return _voicePosition;
}

void AudioPosition::setVoiceRange(int range)
{
    if (range <= 0)
        _voiceRange = 1;
    else 
        _voiceRange = range;
}

int AudioPosition::getVoiceRange()
{
    return _voiceRange;
}

void AudioPosition::toggle3dVoice(bool toggle)
{
    _toggle3dVoice = toggle;
}

bool AudioPosition::isEnabled()
{
    return _toggle3dVoice;
}

double AudioPosition::getDistance(int x0, int y0, int z0, int x1, int y1, int z1)
{
    double dx = x0 - x1;
    double dy = y0 - y1;
    double dz = z0 - z1;

    return std::sqrt(dx * dx + dy * dy + dz * dz);
}