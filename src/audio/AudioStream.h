#pragma once

#include <iostream>
#include <thread>
#include <chrono>
#include <atomic>

#include <mumlib2.h>
#include <portaudio.h>
#include <readerwriterqueue.h>

#include "AudioTargetList.h"

#define SAMPLE_RATE 48000 
#define FRAMES_PER_BUFFER 960

class AudioStream
{
public:
    struct Packet
    {
        int16_t* _buffer = nullptr;
        size_t _bufferSize = FRAMES_PER_BUFFER;
        std::array<float, 3> _position = { 0, 0, 0 };
    };

protected:
    moodycamel::ReaderWriterQueue<AudioStream::Packet> _outputBuffer;

    bool _manualExit;

private:
    std::atomic<bool> _toggleInput;
    std::atomic<bool> _toggleOutput;

    std::atomic<double> _outputVolume;
    std::atomic<double> _inputVolume;

    PaStream* stream = nullptr;
public:
    AudioStream();
    ~AudioStream();

    void exit();

    void inputStreamCallback(const int16_t* inputBuffer, size_t framesPerBuffer);
    void outputStreamCallback(void* outputBuffer, size_t framesPerBuffer);

    void pushToOutputQueue(const int16_t* inputBuffer, size_t sampleCount, const std::array<float, 3>& position);

    void toggleInput(bool toggle);
    void toggleOutput(bool toggle);

    void setOutputVolume(double volume);
    double getOutputVolume();

    void setInputVolume(double volume);
    double getInputVolume();

    static int callback(const void* inputBuffer, void* outputBuffer, unsigned long framesPerBuffer, const PaStreamCallbackTimeInfo* timeInfo, PaStreamCallbackFlags statusFlags, void* userData);

    void init();
};
