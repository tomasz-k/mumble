#include "AudioTargetList.h"
#include "MumClient.h"

AudioTargetList::AudioTargetList() : _isToggled(false), _playerList {}
{
}

AudioTargetList::~AudioTargetList()
{
}

//
// Internal functions, used by core code
//

void AudioTargetList::playerUpdate(int sessionId, int playerId)
{
	std::lock_guard<std::mutex> lock(_mutex);

	auto it = std::find_if(_playerList.begin(), _playerList.end(), [sessionId](const Player& player) {
			return player._sessionId == sessionId;
		});

	if (it == _playerList.end())
	{
		AudioTargetList::Player player;

		player._playerId = playerId;
		player._sessionId = sessionId;

		player._isTarget = false;

		_playerList.push_back(player);
	}
}

void AudioTargetList::playerDisconnect(int sessionId)
{
	std::lock_guard<std::mutex> lock(_mutex);

	auto it = std::remove_if(_playerList.begin(), _playerList.end(), [sessionId](const AudioTargetList::Player& player) {
			return player._sessionId == sessionId;
		});

	if (it == _playerList.end())
		return;

	_playerList.erase(it, _playerList.end());

	MumClient::getInstance()->enqueuePost([sessionId]()
		{
			MumClient::getInstance()->getMumlib()->sendVoiceTargetRemove(mumlib2::VoiceTargetType::USER, sessionId);
		});
}

//
// Extrenal functions, used by squirrel
//

bool AudioTargetList::addVoiceTarget(int playerId)
{
	std::lock_guard<std::mutex> lock(_mutex);

	auto player = std::find_if(_playerList.begin(), _playerList.end(), [playerId](const Player& player) {
			return player._playerId == playerId;
		});

	if (player == _playerList.end() || player->_isTarget)
		return false;
	
	player->_isTarget = true;
		
	int sessionId = player->_sessionId;

	MumClient::getInstance()->enqueuePost([sessionId]()
		{
			MumClient::getInstance()->getMumlib()->sendVoiceTarget(TARGET_ID, mumlib2::VoiceTargetType::USER, sessionId);
		});

	return true;
}

bool AudioTargetList::removeVoiceTarget(int playerId)
{
	std::lock_guard<std::mutex> lock(_mutex);

	auto player = std::find_if(_playerList.begin(), _playerList.end(), [playerId](const Player& player) {
			return player._playerId == playerId;
		});
	
	if (player == _playerList.end() || player->_isTarget == false)
		return false;

	player->_isTarget = false;

	int sessionId = player->_sessionId;

	MumClient::getInstance()->enqueuePost([sessionId]()
		{
			MumClient::getInstance()->getMumlib()->sendVoiceTargetRemove(mumlib2::VoiceTargetType::USER, sessionId);
		});

	return true;
}

//
// Interextrenal functions, used by both core and squirrel
//

void AudioTargetList::mutePlayerVoice(int playerId, bool muted)
{
	std::lock_guard<std::mutex> lock(_mutex);

	auto player = std::find_if(_playerList.begin(), _playerList.end(), [playerId](const Player& player) {
		return player._playerId == playerId;
		});

	if (player == _playerList.end() || player->_isTarget == false)
		return;

	player->_isMuted = muted;
}

bool AudioTargetList::isPlayerMuted(int playerId)
{
	std::lock_guard<std::mutex> lock(_mutex);

	auto player = std::find_if(_playerList.begin(), _playerList.end(), [playerId](const Player& player) {
		return player._playerId == playerId;
		});

	if (player != _playerList.end()) {
		return player->_isMuted;
	}

	return false;
}

std::vector<int> AudioTargetList::getTargetList() const
{
	std::lock_guard<std::mutex> lock(_mutex);

	std::vector<int> _targetList = {};

	for (const auto& player : _playerList)
	{
		if (player._isTarget == true)
		{
			_targetList.push_back(player._playerId);
		}
	}

	return _targetList;
}

//
// Extrenal functions, used by squirrel
//

void AudioTargetList::toggle(bool value)
{
	_isToggled = value;
}

bool AudioTargetList::isToggled()
{
	return _isToggled;
}
