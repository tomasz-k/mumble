#include "sq_callback.h"

SQInteger sq_onRender(HSQUIRRELVM vm)
{
    MumClient::getInstance()->executeAsync();
    return 0;
}

SQInteger sq_onExit(HSQUIRRELVM vm)
{
    MumClient::getInstance()->exit();
    return 0;
}