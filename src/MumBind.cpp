#include "MumBind.h"
#include "MumClient.h"

void MumBind::addEvent(const char* eventName)
{
    Sqrat::Function sq_addEvent = Sqrat::RootTable().GetFunction("addEvent");

    if (sq_addEvent.IsNull())
        return;

    sq_addEvent(eventName);
}

void MumBind::bind()
{
    using namespace SqModule;

    /* squirreldoc (event)
    *
    * This event is triggered every time when someone voice is received by client. If param isLast is true, It means last buffer frame was played.
    *
    * @version	0.1
    * @side		client
    * @name		mumble:onPlayerTalking
    * @param	(int) playerId.
    * @param	(bool) isLast. 
    *
    */

    addEvent("mumble:onPlayerTalking");

    /* squirreldoc (class)
    *
    * This static class represents mumble namespace.
    *
    * @version	0.1
    * @side		client
    * @name		mumble
    *
    */

    Sqrat::RootTable().Bind("mumble", Sqrat::Table(vm)
        .SquirrelFunc("openConnection", &MumBind::openConnection, 4)
        .SquirrelFunc("closeConnection", &MumBind::closeConnection, 1)
        .SquirrelFunc("joinChannel", &MumBind::joinChannel, 2)
        .SquirrelFunc("toggleInput", &MumBind::toggleInput, 2)
        .SquirrelFunc("toggleOutput", &MumBind::toggleOutput, 2)
        .SquirrelFunc("setOutputVolume", &MumBind::setOutputVolume, 2)
        .SquirrelFunc("getOutputVolume", &MumBind::getOutputVolume, 1)
        .SquirrelFunc("getInputVolume", &MumBind::getInputVolume, 1)
        .SquirrelFunc("addVoiceTarget", &MumBind::addVoiceTarget, 2)
        .SquirrelFunc("removeVoiceTarget", &MumBind::removeVoiceTarget, 2)
        .SquirrelFunc("getVoiceTargets", &MumBind::getVoiceTargets, 1)
        .SquirrelFunc("toggleTargetList", &MumBind::toggleTargetList, 2)
        .SquirrelFunc("isTargetListToggled", &MumBind::isTargetListToggled, 1)
        .SquirrelFunc("setVoicePosition", &MumBind::setVoicePosition, 4)
        .SquirrelFunc("setVoiceRange", &MumBind::setVoiceRange, 2)
        .SquirrelFunc("toggle3dVoice", &MumBind::toggle3dVoice, 2)
        .SquirrelFunc("is3dVoiceEnabled", &MumBind::is3dVoiceEnabled, 1)
        .SquirrelFunc("mutePlayer", &MumBind::mutePlayer, 2));
}

/* squirreldoc (method)
*
* This function is used to open a connection to a Mumble server.
*
* @version	0.1
* @name		openConnection
* @param	(string) Mumble server IP/domain address.
* @param	(int) Mumble server port.
* @param	(string) Password to the Mumble server, if there is no password, set to "".
* @return	(bool) Returns true if connection attempt has started successfully, otherwise returns false.
*
*/

SQInteger MumBind::openConnection(HSQUIRRELVM vm)
{
    const SQChar* password;
    sq_getstring(vm, 4, &password);

    SQInteger port;
    sq_getinteger(vm, 3, &port);

    const SQChar* serverIp;
    sq_getstring(vm, 2, &serverIp);

    Sqrat::PushVar(vm, MumClient::getInstance()->openConnection(serverIp, port, password));
    return 1;
}

/* squirreldoc (method)
*
* This function is used to close a connection to a Mumble server.
*
* @version	0.1
* @name		closeConnection
* @return	(bool) Returns true if connection close attempt has been successful.
*
*/

SQInteger MumBind::closeConnection(HSQUIRRELVM vm)
{
    Sqrat::PushVar(vm, MumClient::getInstance()->closeConnection());
    return 1;
}

/* squirreldoc (method)
*
* This function is used to join a specific channel on your Mumble server.
*
* @version	0.1
* @name		joinChannel
* @param	(string) channel, remember that the channel must be created before you try to join.
*
*/

SQInteger MumBind::joinChannel(HSQUIRRELVM vm)
{
    const SQChar* channel;
    sq_getstring(vm, 2, &channel);

    MumClient::getInstance()->joinChannel(channel);

    return 1;
}

/* squirreldoc (method)
*
* This function is used to enable/disable audio input.
*
* @version	0.1
* @name		toggleInput
* @param    (bool) toggle
*
*/

SQInteger MumBind::toggleInput(HSQUIRRELVM vm)
{
    SQBool toggle;
    sq_getbool(vm, 2, &toggle);

    MumClient::getInstance()->toggleInput(toggle);

    return 1;
}

/* squirreldoc (method)
*
* This function is used to enable/disable audio output.
*
* @version	0.1
* @name		toggleOutput
* @param	(bool) toggle
*
*/

SQInteger MumBind::toggleOutput(HSQUIRRELVM vm)
{
    SQBool toggle;
    sq_getbool(vm, 2, &toggle);

    MumClient::getInstance()->toggleOutput(toggle);

    return 1;
}

/* squirreldoc (method)
*
* This function is used to set the output volume. You can set a value from 0.0 to 1.0.
*
* @version	0.1
* @name		setOutputVolume
* @param    (float) volume
*
*/

SQInteger MumBind::setOutputVolume(HSQUIRRELVM vm)
{
    SQFloat volume;
    sq_getfloat(vm, 2, &volume);

    MumClient::getInstance()->setOutputVolume(volume);

    return 1;
}

/* squirreldoc (method)
*
* This function is used to get the current output volume (default is 1.0).
*
* @version	0.1
* @name		getOutputVolume
* @return	(float) Returns the current output volume.
*
*/

SQInteger MumBind::getOutputVolume(HSQUIRRELVM vm)
{
    Sqrat::PushVar(vm, MumClient::getInstance()->getOutputVolume());
    return 1;
}

/* squirreldoc (method)
*
* This function is used to get the current input (microphone) volume represented by the RMS value (https://en.wikipedia.org/wiki/Root_mean_square).
*
* @version	0.1
* @name		getInputVolume
* @return	(float) Returns the current input volume.
*
*/

SQInteger MumBind::getInputVolume(HSQUIRRELVM vm)
{
    Sqrat::PushVar(vm, MumClient::getInstance()->getInputVolume());
    return 1;
}

/* squirreldoc (method)
*
* This function is used to add voice target to list.
*
* @version	0.1
* @name		addVoiceTarget
* @param    (int) playerId
*
*/

SQInteger MumBind::addVoiceTarget(HSQUIRRELVM vm)
{
    SQInteger playerId;
    sq_getinteger(vm, 2, &playerId);

    MumClient::getInstance()->addVoiceTarget(playerId);

    return 1;
}

/* squirreldoc (method)
*
* This function is used to remove voice target from list.
*
* @version	0.1
* @name		removeVoiceTarget
* @param    (int) playerId
*
*/

SQInteger MumBind::removeVoiceTarget(HSQUIRRELVM vm)
{
    SQInteger playerId;
    sq_getinteger(vm, 2, &playerId);

    MumClient::getInstance()->removeVoiceTarget(playerId);

    return 1;
}

/* squirreldoc (method)
*
* This function is used to get list of all added by user voice targets.
*
* @version	0.1
* @name		getVoiceTargets
* @return	(array) returns array of all current voice targets(in form of player ids).
*
*/

SQInteger MumBind::getVoiceTargets(HSQUIRRELVM vm)
{
    Sqrat::Array targetList(vm);
    
    std::vector<int> _targetList = MumClient::getInstance()->getVoiceTargets();

    for (const auto& targetId : _targetList)
    {
        targetList.Append(targetId);
    }

    Sqrat::PushVar(vm, targetList);

    return 1;
}

/* squirreldoc (method)
*
* This function is used to enable/disable target list system.
*
* @version	0.1
* @name		toggleTargetList
*
*/

SQInteger MumBind::toggleTargetList(HSQUIRRELVM vm)
{
    SQBool toggle;
    sq_getbool(vm, 2, &toggle);

    MumClient::getInstance()->toggleTargetList(toggle);

    return 1;
}

/* squirreldoc (method)
*
* This function is used to check if target list system is disabled/enabled.
*
* @version	0.1
* @name		isTargetListToggled
* @return	(bool) returns true if target list system is enabled, otherwise returns false.
*
*/

SQInteger MumBind::isTargetListToggled(HSQUIRRELVM vm)
{
    Sqrat::PushVar(vm, MumClient::getInstance()->isTargetListToggled());
    return 1;
}

/* squirreldoc (method)
*
* his function is used to determine your voice position, which is later utilized by a module to ascertain where you are speaking from (position data is sent along with your voice) and what you are hearing (the voice's position determines what you are hearing). 
* You should regularly set the position, typically in events like onRender, to provide accurate data.
* 
* @version	0.1
* @name		setVoicePosition
* @param    (float) x Position x of player.
* @param    (float) y Position y of player.
* @param    (float) z Position z of player.
*
*/

SQInteger MumBind::setVoicePosition(HSQUIRRELVM vm)
{
    SQFloat positionZ;
    sq_getfloat(vm, 4, &positionZ);

    SQFloat positionY;
    sq_getfloat(vm, 3, &positionY);

    SQFloat positionX;
    sq_getfloat(vm, 2, &positionX);

    MumClient::getInstance()->setVoicePosition(positionX, positionY, positionZ);

    return 1;
}

/* squirreldoc (method)
*
* This function is used to set voice range(maximum hearing range).
*
* @version	0.1
* @name		setVoiceRange
* @param	(int) range
*
*/

SQInteger MumBind::setVoiceRange(HSQUIRRELVM vm)
{
    SQInteger range;
    sq_getinteger(vm, 2, &range);

    MumClient::getInstance()->setVoiceRange(range);

    return 1;
}

/* squirreldoc (method)
*
* This function is used to enable/disable 3d voice.
*
* @version	0.1
* @name		toggle3dVoice
* @param	(bool) toggle
*
*/

SQInteger MumBind::toggle3dVoice(HSQUIRRELVM vm)
{
    SQBool toggle;
    sq_getbool(vm, 2, &toggle);

    MumClient::getInstance()->toggle3dVoice(toggle);

    return 1;
}

/* squirreldoc (method)
*
* This function returns information about 3d voice function status.
*
* @version	0.1
* @name		is3dVoiceEnabled
* @return	(bool) returns true if 3d voice is enabled otherwise returns false.
*
*/

SQInteger MumBind::is3dVoiceEnabled(HSQUIRRELVM vm)
{
    Sqrat::PushVar(vm, MumClient::getInstance()->is3dVoiceEnabled());
    return 1;
}

/* squirreldoc (method)
*
* This function mutes player with given id.
* * @param	(int) playerId
* @param	(bool) toggle mute or unmute
*
* @version	0.1
* @name		mutePlayer
*
*/

SQInteger MumBind::mutePlayer(HSQUIRRELVM vm)
{
    SQBool mute;
    sq_getbool(vm, 3, &mute);

    SQInteger playerId;
    sq_getinteger(vm, 2, &playerId);

    MumClient::getInstance()->mutePlayer(playerId, mute);
    return 1;
}